# irisLocalServer

语言: go语言  
编译: 多平台交叉编译 仅测试过win7 64系统 其他平台自测 or自己拿代码跑 ,装一套go环境就能跑,代码本身很简单,没有依赖第三方框架  
原作者: https://www.52pojie.cn/thread-690218-1-1.html  
原作者源码地址: https://github.com/ercJuL/IrisProxyCracked  

2018年11月15日16:34:18 更新
交叉编译可以使用gox  https://github.com/mitchellh/gox

下载地址:
https://gitee.com/ilaotan/irisLocalServer/releases/1.0.0



log.Println("本软件仅限用于学习和研究目的")  
log.Println("本软件会占用127.0.0.1:80端口,若启动失败,请自行检查")  
log.Println("首发www.52pojie.cn 感谢erchioce的本地破解思路")  
log.Println("由 依然小圣 提供基于go语言编译的win linux等平台下可执行文件,不放心者可拿本代码自己编译.....")  
log.Println("====服务启动成功====")  
log.Println("====请按以下说明进行操作====")  
log.Println("0.修改C:\\Windows\\System32\\drivers\\etc\\hosts文件,末尾添加 127.0.0.1 iristech.co")  
log.Println("1.在Iris中输入任意激活码激活(我测试时没到激活码那就直接激活了,可能之前装过的原因)")  
log.Println("2.看到本程序有SUCCESS输出时,大体就成功了")  
log.Println("3.激活成功后关闭本窗口")  