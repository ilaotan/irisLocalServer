package main

import (
	"net/http"
	"log"
)

func main() {

	http.HandleFunc("/", allThing)

	log.Println("本软件仅限用于学习和研究目的")
	log.Println("本软件会占用127.0.0.1:80端口,若启动失败,请自行检查")
	log.Println("首发www.52pojie.cn 感谢erchioce的本地破解思路")
	log.Println("由 依然小圣 提供基于go语言编译的win linux等平台下可执行文件,不放心者可拿本代码自己编译.....")
	log.Println("====服务启动成功====")
	log.Println("====请按以下说明进行操作====")
	log.Println("0.修改C:\\Windows\\System32\\drivers\\etc\\hosts文件,末尾添加 127.0.0.1 iristech.co")
	log.Println("1.在Iris中输入任意激活码激活(我测试时没到激活码那就直接激活了,可能之前装过的原因)")
	log.Println("2.看到本程序有SUCCESS输出时,大体就成功了")
	log.Println("3.激活成功后关闭本窗口")
	log.Fatal(http.ListenAndServe(":80", nil))
}

func allThing(w http.ResponseWriter, r *http.Request) {
	if r.Method== "GET" {
		log.Println("获得 GET 请求 返回 SUCCESS")
		log.Println(r.URL.Query())
		w.Write([]byte("SUCCESS"))
	}else {
		log.Println("获得 POST 请求 返回 OK")
		log.Println(r.URL.Query())
		w.Write([]byte("OK"))
	}
}